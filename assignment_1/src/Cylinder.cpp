//=============================================================================
//
//   Exercise code for the lecture
//   "Introduction to Computer Graphics"
//   by Prof. Dr. Mario Botsch, Bielefeld University
//
//   Copyright (C) Computer Graphics Group, Bielefeld University.
//
//=============================================================================

//== INCLUDES =================================================================

#include "Cylinder.h"
#include "SolveQuadratic.h"

#include <array>
#include <cmath>

//== IMPLEMENTATION =========================================================

bool
Cylinder::
intersect(const Ray&  _ray,
          vec3&       _intersection_point,
          vec3&       _intersection_normal,
          double&     _intersection_t) const
{
  /*
    implicit equation: |x-c|² = r² + dot(x-c, a)²
      o: origin
      c: center
      a: direction of axis
      r: radius

    x = o + td  =>  |o + td -c|² = r² + dot(o+td-c, a)²
    dot(o+td-c, o+td-c) = r² + (dot(o-c,a) + t*dot(d,a))²

    dot(o+td-c, o-c) + dot(o+td-c, td) 
      = dot(o-c, o-c) + t*dot(d, o-c) + t*dot(o+td-c, d)
    dot(o+td-c, d) = dot(o-c, d) + t*dot(d, d)

    => dot(o-c, o-c) + 2*t*dot(d, o-c) + t²*dot(d, d)

    dot(o-c, a)² + 2*t*dot(d,a)*dot(o-c, a) + t²*dot(d,a)²

    ==>dot(o-c,o-c)+2*t*dot(d,o-c)+t²*dot(d, d)-r²-dot(o-c,a)²-2*t*dot(d,a)*dot(o-c,a)-t²*dot(d,a)²
     = t²*(dot(d,d)-dot(d,a)²) + 2*t*(dot(d,o-c)-dot(d,a)*dot(o-c,a)) + dot(o-c,o-c)-r²-dot(o-c,a)²
    

   */

  const vec3 &d = _ray.direction;
  const vec3 oc = _ray.origin - center;
  const vec3 a = axis;
  const double r = radius;
  std::array<double, 2> t;

  size_t number_of_solutions = solveQuadratic(dot(d,d)-dot(d,a)*dot(d,a),
					      2*(dot(d,oc)-dot(d,a)*dot(oc,a)),
					      dot(oc,oc)-r*r-dot(oc,a)*dot(oc,a),
					      t);

  _intersection_t = NO_INTERSECTION;

  for (size_t i = 0; i < number_of_solutions; i++) {
    const double t_solution = t[i];    
    
    // t <= 0 means the intersection is behind the camera
    if ((t_solution > 0) && (t_solution < _intersection_t)) {
      _intersection_point = _ray(t_solution);
      const double point_height = dot(_intersection_point - center,a);
      
      //test if intersection is in the height of cylinder if it is update intersection
      if(point_height >= -height/2 && point_height <= height/2){
	_intersection_t = t_solution;
	const vec3 intersection_axis_point = center+point_height*a;
	_intersection_normal = (_ray(_intersection_t)-intersection_axis_point)/radius;
	if(i == 0) _intersection_normal*=-1;
      }
    }
  }

  if (_intersection_t == NO_INTERSECTION) return false;


  return true;
}
