Plane:
-------

Explanation:

We solve the following equation given in the lecture :
  dot(n, x - c) = 0
  dot(n, o+td - c) = 0
  dot(n,o) + dot(n,td) - dot(n,c) = 0
  t * dot(n, d) = dot(n,c) - dot(n,o)
  t = (dot(n,c) - dot(n,o)) / dot(n,d)

We verify that there is an intersection, i.e. dot(n,d) /= 0
We compute t and verify we have an intersection after the origin of the ray
i.e. t > 0

We return the point of intersection ; the normal to the plane is its own
normal, which we return.


Cylinder:
----------

* Short explanation:
We used the equation for the distance between a point and a line and set it to be
equal to the radius of the cylinder.

We started by resolving the equation for an infinitly long cylinder and once we got the
two solutions we check if they are at the right height (between -height/2 and height/2)

To compute the normal we used the intersection point we found and project the vector from
the center to it into the axis vector. This give us the radius at this point that we finaly
need to normalize. To have the normal in the right direction for the iner faces we just have to multiply the normal obtained by -1


* Problem encountered:
We first discarded both solution if the smaller one was not in the height of the cylinder (we did
not check it for each intersection but only at the end)
We first thought that the center of the cylinder was at the bottom (center of the circle) and not
the real center.


________________________________________________________________

Workload: 1/3 for each of us, we splited the work equally

Sources:
Webpage in which we found the equation for distance point to line:
   https://math.stackexchange.com/questions/1905533/find-perpendicular-distance-from-point-to-line-in-3d
