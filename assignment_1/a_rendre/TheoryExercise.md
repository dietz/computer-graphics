# Theory Exercise
``A TheoryExercise.pdf containing your cylinder-ray intersection and normal
derivations.``

## Cylinder-ray inteserction

We start from the following formula :

``|x-c|² = r² + dot(x-c, a)^2``

x is equal to o + td.

With :

  - o the origin of the ray
  - d its direction
  - c the center of the cylinder
  - a the direction of the cylinder
  - r its radius

We insert the ray's equation into the equation :

`` dot(o+td-c, o+td-c) = r² + (dot(o-c,a) + t*dot(d,a))²``

We develop the left part :

  ``dot(o+td-c, o-c) + dot(o+td-c, td) = dot(o-c, o-c) + t*dot(d, o-c) + t*dot(o+td-c, d)
  dot(o+td-c, d) = dot(o-c, d) + t*dot(d, d)``

We obtain :

``dot(o-c, o-c) + 2*t*dot(d, o-c) + t²*dot(d, d)``

We then develop parts of the right part :

``dot(o-c, a)² + 2*t*dot(d,a)*dot(o-c, a) + t²*dot(d,a)²``

And finally we put everything together

  ``dot(o-c,o-c)+2*t*dot(d,o-c)+t²*dot(d, d)-r²-dot(o-c,a)²-2*t*dot(d,a)*dot(o-c,a)-t²*dot(d,a)²
     = t²*(dot(d,d)-dot(d,a)²) + 2*t*(dot(d,o-c)-dot(d,a)*dot(o-c,a)) + dot(o-c,o-c)-r²-dot(o-c,a)²``

We then resolve this quadratic into ``Cylinder.cpp``


## Normal derivation

We compute the intersection point, its height :

``_intersection_point = o + d*t_solution``
``point_height = dot(_intersection_point - center, a)``

If the height is between the bonds of the cylinder we consider the intersection
axis :


``intersection_axis_point = c+point_height*a``

We then compute the  normal :

``_intersection_normal = (_intersection_point-intersection_axis_point)/radius``

If the t_solution used is the first one we found from the quadratic, we
multiply the normal by -1 to make it face the watcher.
