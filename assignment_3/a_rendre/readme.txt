# README
Work was done equally between everyone

## Vertex Normals
This part was really straightforward, we just had to follow the instructions
and the formulas. We just had to be careful to the function ``angleWeights´´
which uses argument pointers to deliver its result.


## Ray-triangle Intersection
This seconde part had to difficulties :
  - We had to manipulate the intersection formula so we could use Cramer's
    rule. It was done the following way :

    * Express alpha*A + beta*B + gamma*C as Dx such that D = |ABC| and
      x = |alpha beta gamma|^T
    * Express b = |o + t*d|
    => We use Cramer's rule on Dx = b

  - We had to implement a function det() which computes the determinant of a
    3x3 matrix


## Efficient Ray-Mesh Intersections
This part was really difficult, the solution is the following :

Each bounding box has two points bb_min and bb_max which contain 3 coordinates.
The idea is to project these points on each of the axis to obtain 3 pairs of
points of two coordinates.
These points delimit lines parallel to each axis. They intersect and form a
square :

                    y axis
                    -   -
                    -   -
                    -   -
            --------====*--------
                    =   =
                    =   =
                    =   =
            --------*====-------- x axis
                    -   -
                    -   -
                    -   -

It is then easy to compute the intersection between each line and the ray.
=> the ray is defined by O.x + t*D.x and O.y + t*D.y and the lines by

  - y = bb_min.x
  - y = bb_max.x these two have to be equal to O.x + t*D.x
  - x = bb_min.y
  - x = bb_min.y these two have to be equal to O.y + t*D.y

We then have tx_min, tx_max, ty_min and ty_max. The ray intersects with the
square at t_min = max(tx_min, ty_min) and t_max = min(tx_max, ty_max).

Once we have tx_min, tx_max, ty_min and ty_max we must make sure the
intersection to the lines lies inside the square. It is the case if :
  - tx_min < ty_min
  - tx_max > ty_max

===> Apply this method to the pairs of axis x/y, x/z and y/z.




















