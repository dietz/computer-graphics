//=============================================================================
//
//   Exercise code for the lecture
//   "Introduction to Computer Graphics"
//   by Prof. Dr. Mario Botsch, Bielefeld University
//
//   Copyright (C) Computer Graphics Group, Bielefeld University.
//
//=============================================================================

//== INCLUDES =================================================================

#include "Mesh.h"
#include <fstream>
#include <string>
#include <stdexcept>
#include <limits>
#include <cmath>   // needed for double version of std::abs

//== IMPLEMENTATION ===========================================================


Mesh::Mesh(std::istream &is, const std::string &scenePath)
{
    std::string meshFile, mode;
    is >> meshFile;

    // load mesh from file
    read(scenePath.substr(0, scenePath.find_last_of("/\\") + 1) + meshFile); // Use both Unix and Windows path separators

    is >> mode;
    if      (mode ==  "FLAT") draw_mode_ = FLAT;
    else if (mode == "PHONG") draw_mode_ = PHONG;
    else throw std::runtime_error("Invalid draw mode " + mode);

    is >> material;
}


//-----------------------------------------------------------------------------


bool Mesh::read(const std::string &_filename)
{
    // read a mesh in OFF format


    // open file
    std::ifstream ifs(_filename);
    if (!ifs)
    {
        std::cerr << "Can't open " << _filename << "\n";
        return false;
    }


    // read OFF header
    std::string s;
    unsigned int nV, nF, dummy, i;
    ifs >> s;
    if (s != "OFF")
    {
        std::cerr << "No OFF file\n";
        return false;
    }
    ifs >> nV >> nF >> dummy;
    std::cout << "\n  read " << _filename << ": " << nV << " vertices, " << nF << " triangles";


    // read vertices
    Vertex v;
    vertices_.clear();
    vertices_.reserve(nV);
    for (i=0; i<nV; ++i)
    {
        ifs >> v.position;
        vertices_.push_back(v);
    }


    // read triangles
    Triangle t;
    triangles_.clear();
    triangles_.reserve(nF);
    for (i=0; i<nF; ++i)
    {
        ifs >> dummy >> t.i0 >> t.i1 >> t.i2;
        triangles_.push_back(t);
    }


    // close file
    ifs.close();


    // compute face and vertex normals
    compute_normals();

    // compute bounding box
    compute_bounding_box();


    return true;
}


//-----------------------------------------------------------------------------

// Determine the weights by which to scale triangle (p0, p1, p2)'s normal when
// accumulating the vertex normals for vertices 0, 1, and 2.
// (Recall, vertex normals are a weighted average of their incident triangles'
// normals, and in our raytracer we'll use the incident angles as weights.)
// \param[in] p0, p1, p2    triangle vertex positions
// \param[out] w0, w1, w2    weights to be used for vertices 0, 1, and 2
void angleWeights(const vec3 &p0, const vec3 &p1, const vec3 &p2,
                  double &w0, double &w1, double &w2) {
    // compute angle weights
    const vec3 e01 = normalize(p1-p0);
    const vec3 e12 = normalize(p2-p1);
    const vec3 e20 = normalize(p0-p2);
    w0 = acos( std::max(-1.0, std::min(1.0, dot(e01, -e20) )));
    w1 = acos( std::max(-1.0, std::min(1.0, dot(e12, -e01) )));
    w2 = acos( std::max(-1.0, std::min(1.0, dot(e20, -e12) )));
}


//-----------------------------------------------------------------------------

void Mesh::compute_normals()
{
    // compute triangle normals
    for (Triangle& t: triangles_)
    {
        const vec3& p0 = vertices_[t.i0].position;
        const vec3& p1 = vertices_[t.i1].position;
        const vec3& p2 = vertices_[t.i2].position;
        t.normal = normalize(cross(p1-p0, p2-p0));
    }

    // initialize vertex normals to zero
    for (Vertex& v: vertices_)
    {
        v.normal = vec3(0,0,0);
    }

    /** \todo
     * In some scenes (e.g the office scene) some objects should be flat
     * shaded (e.g. the desk) while other objects should be Phong shaded to appear
     * realistic (e.g. chairs). You have to implement the following:
     * - Compute vertex normals by averaging the normals of their incident triangles.
     * - Store the vertex normals in the Vertex::normal member variable.
     * - Weigh the normals by their triangles' angles.
     */


    for(Triangle& t : triangles_){

      Vertex& v0 = vertices_[t.i0];
      Vertex& v1 = vertices_[t.i1];
      Vertex& v2 = vertices_[t.i2];
      
      double w[3];
      angleWeights(v0.position , v1.position , v2.position , w[0] ,w[1] , w[2]);
      
      v0.normal += t.normal*w[0];
      v1.normal += t.normal*w[1];
      v0.normal += t.normal*w[2];
    }

    // normalise vertices normal
    for (Vertex& v: vertices_)
    {
      v.normal = normalize(v.normal);
    }
}


//-----------------------------------------------------------------------------


void Mesh::compute_bounding_box()
{
    bb_min_ = vec3(std::numeric_limits<double>::max());
    bb_max_ = vec3(std::numeric_limits<double>::lowest());

    for (Vertex& v: vertices_)
    {
        bb_min_ = min(bb_min_, v.position);
        bb_max_ = max(bb_max_, v.position);
    }
}


//-----------------------------------------------------------------------------

const vec3 operator/(const vec3 a, const vec3 b){
  return vec3(a[0]/b[0], a[1]/b[1], a[2]/b[2]);
}

bool Mesh::intersect_bounding_box(const Ray& _ray) const
{

    /** \todo
    * Intersect the ray `_ray` with the axis-aligned bounding box of the mesh.
    * Note that the minimum and maximum point of the bounding box are stored
    * in the member variables `bb_min_` and `bb_max_`. Return whether the ray
    * intersects the bounding box.
    * This function is ued in `Mesh::intersect()` to avoid the intersection test
    * with all triangles of every mesh in the scene. The bounding boxes are computed
    * in `Mesh::compute_bounding_box()`.
    */
  
  const vec3 min = (bb_min_ - _ray.origin)/_ray.direction;
  const vec3 max = (bb_max_ - _ray.origin)/_ray.direction;

  double txmin, txmax;
  if(min[0] < max[0]){
    txmin = min[0];
    txmax = max[0];
  }else{
    txmin = max[0];
    txmax = min[0];
  }

  double tymin, tymax;
  if(min[1] < max[1]){
    tymin = min[1];
    tymax = max[1];
  }else{
    tymin = max[1];
    tymax = min[1];
  }

  double tzmin, tzmax;
  if(min[2] < max[2]){
    tzmin = min[2];
    tzmax = max[2];
  }else{
    tzmin = max[2];
    tzmax = min[2];
  }

  if(txmin>tymax || tymin>txmax) return false;

  if(tymin > txmin) txmin = tymin;
  if(tymax < txmax) txmax = tymax;

  if(txmin > tzmax || tzmin > txmax) return false;

  return true;
}


//-----------------------------------------------------------------------------


bool Mesh::intersect(const Ray& _ray,
                     vec3&      _intersection_point,
                     vec3&      _intersection_normal,
                     double&    _intersection_t ) const
{
    // check bounding box intersection
    if (!intersect_bounding_box(_ray))
    {
        return false;
    }

    vec3   p, n;
    double t;

    _intersection_t = NO_INTERSECTION;

    // for each triangle
    for (const Triangle& triangle : triangles_)
    {
        // does ray intersect triangle?
        if (intersect_triangle(triangle, _ray, p, n, t))
        {
            // is intersection closer than previous intersections?
            if (t < _intersection_t)
            {
                // store data of this intersection
                _intersection_t      = t;
                _intersection_point  = p;
                _intersection_normal = n;
            }
        }
    }

    return (_intersection_t != NO_INTERSECTION);
}


//-----------------------------------------------------------------------------

double
det(const double a11, const double a12, const double a13,
    const double a21, const double a22, const double a23,
    const double a31, const double a32, const double a33 )
{
  double d1 = a11*(a22*a33-a32*a23);
  double d2 = a21*(a12*a33-a32*a13);
  double d3 = a31*(a12*a23-a22*a13);

  return d1-d2+d3;
}

bool
Mesh::
intersect_triangle(const Triangle&  _triangle,
                   const Ray&       _ray,
                   vec3&            _intersection_point,
                   vec3&            _intersection_normal,
                   double&          _intersection_t) const
{
    const vec3& p0 = vertices_[_triangle.i0].position;
    const vec3& p1 = vertices_[_triangle.i1].position;
    const vec3& p2 = vertices_[_triangle.i2].position;

    /** \todo
    * - intersect _ray with _triangle
    * - store intersection point in `_intersection_point`
    * - store ray parameter in `_intersection_t`
    * - store normal at intersection point in `_intersection_normal`.
    * - Depending on the member variable `draw_mode_`, use either the triangle
    *  normal (`Triangle::normal`) or interpolate the vertex normals (`Vertex::normal`).
    * - return `true` if there is an intersection with t > 0 (in front of the viewer)
    *
    * Hint: Rearrange `ray.origin + t*ray.dir = a*p0 + b*p1 + (1-a-b)*p2` to obtain a solvable
    * system for a, b and t.
    * Refer to [Cramer's Rule](https://en.wikipedia.org/wiki/Cramer%27s_rule) to easily solve it.
     */

    /*
      o + t*d = a*p0 + b*p1 + (1-a-b)*p2
    => t*d - a*p0 - b*p1 + a*p2 + b*p2 = p2 - o
    => t*d + a*(p2-p0) + b*(p2-p1) = p2 - o
     */
    
    const vec3& o = _ray.origin;
    const vec3& d = _ray.direction;

    const double a11 = d[0];
    const double a12 = p2[0]-p0[0];
    const double a13 = p2[0]-p1[0];
    const double a21 = d[1];
    const double a22 = p2[1]-p0[1];
    const double a23 = p2[1]-p1[1];
    const double a31 = d[2];
    const double a32 = p2[2]-p0[2];
    const double a33 = p2[2]-p1[2];

    const double b1 = p2[0]-o[0];
    const double b2 = p2[1]-o[1];
    const double b3 = p2[2]-o[2];
    
    double da = det(a11, a12, a13,
		    a21, a22, a23,
		    a31, a32, a33);
    if(da == 0) return false;
    
    double t = det(b1, a12, a13,
		   b2, a22, a23,
		   b3, a32, a33)/da;
    double a = det(a11, b1, a13,
		   a21, b2, a23,
		   a31, b3, a33)/da;
    double b = det(a11, a12, b1,
		   a21, a22, b2,
		   a31, a32, b3)/da;
    double c = 1-a-b;

    _intersection_t = NO_INTERSECTION;

    if(a<0 || b<0 || c<0 || t<0) return false;

    _intersection_t = t;
    _intersection_point = _ray(t);

    if(draw_mode_ == FLAT){
      _intersection_normal = _triangle.normal;
    }else{//draw_mode == PHONG
      const vec3& na = vertices_[_triangle.i0].normal;
      const vec3& nb = vertices_[_triangle.i1].normal;
      const vec3& nc = vertices_[_triangle.i2].normal;

      _intersection_normal = a*na + b*nb + c*nc;
      _intersection_normal = normalize(_intersection_normal);
    }

    return true;
}


//=============================================================================
