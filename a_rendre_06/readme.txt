# Exercise 6
Everyone did their part.
A student thinks that videos of the scene would have helped us.

## Task 4.1

### 4.1.0
#### 1.
Nothing to say here, we had to adjust the code.
#### 2.
Here we had to be careful to un-normalized vectors.

### 4.1.1
For cube_camera_projection we had to find the field of view and the ratio.
Since we are operating on a cube the ration was 1.
The field of view was the angle necessary to go from one edge of the cube's
face to the other, while standing in the middle of the cube : 90 degrees.
### 4.1.2
The difficulty here was to think about the "up" vector : it changes with the
direction the camera is looking at : if the camera looks at "y+" then "up = -z".
We wrote a helper function to make the code more readable.


## Task 4.2

### 4.2.1
Here we just had to understand that our point of view was the light,
hence that its position was (0,0,0).

### 4.2.2
The main difficulty here was to understand that the grey scale we extract from
"shadow_cubemap" is of the same type and order as the length we must compare it to.

### 4.2.3
With the help of the website a TA provided we could understant that "src" and "dst" were added,
and we just had to specify by which factor they were multiplied/divided/etc... And we only had
to add these two values.
