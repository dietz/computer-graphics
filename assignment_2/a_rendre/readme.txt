# How we solved the exercises

## Phong Lighting Model and Shadows
The implementation was quite straight forward as we mainly had to translate
the formula into code. For the shadows we first forgot to add an offset
but once we saw the result it recalled us the trick explained during the
course.

## Reflections
The main difficulty with this part, just like the others, is to attain a good
grasp of the project's structure and its entities. Then we just have to put
everything together with our understanding of the lectures. There could have
been a tricky part with the offset but we were warned in the slides.
It was sufficient to add a extremly reduced ´´normal´´ (i.e. multiplied by
´´10^-9´´) and adding it to the intersection point.


# Job repartition: we split the work equally: 1/3 for each of us
