//=============================================================================
//
//   Exercise code for the lecture
//   "Introduction to Computer Graphics"
//   by Prof. Dr. Mario Botsch, Bielefeld University
//
//   Copyright (C) Computer Graphics Group, Bielefeld University.
//
//=============================================================================


//== INCLUDES =================================================================

#include "Plane.h"
#include <limits>


//== CLASS DEFINITION =========================================================



Plane::Plane(const vec3& _center, const vec3& _normal)
: center(_center), normal(_normal)
{
}


//-----------------------------------------------------------------------------


bool
Plane::
intersect(const Ray& _ray,
          vec3&      _intersection_point,
          vec3&      _intersection_normal,
          double&    _intersection_t ) const
{
  /*
    Solving using the equation
      n(o+td-c) = 0
    for intersection distance "t", where
      o = origin
      d = ray.direction
      c = center

    => no + nd*t - nc = 0
    => t = n(c - o)/nd
  */

  double nd = dot(normal, _ray.direction);
  
  _intersection_t = NO_INTERSECTION;

  //if plane parallel to dir then no intersection
  if(nd == 0) return false;
  
  _intersection_t = dot(normal, center - _ray.origin) / nd;

  //if intersection before origin then no intersection after origin
  if(_intersection_t < 0){
    _intersection_t = NO_INTERSECTION;
    return false;
  }
  
  _intersection_point = _ray(_intersection_t);
  _intersection_normal = normal;

  return true;

}
//=============================================================================
