

# Task 3.1    billboarding

SunBillboardActor 's mat_model_to_world = mat_scale * lookAt
where lookAt is a tranform matrix computed using :  
        mat4.targetTo ( ... , [0,0,0] ,camera_position ,[0,0,1] ) 
, so that the billboard face the camera.

For blending we enabled it within the init_pipeline (just copied the link given).

To compute the billboard color within the fragment shader ,
we compute the euclidian distance : 
        dist = distance( v2f_tex_coord , vec2(0) )

To finish to compute gl_FragColor we opted for : 
        gl_FragColor = vec4( glow_color , pow( 1.0 - dist , 3.0) )

We tested other formulas . but kept that one because the light around the sun was not too bright.




# Task 3.2    Phong lighting model

In the vertex shader we computed : 
    v2f_dir_from_view =  - ( mat_model_view * vec4(position, 1) ).xyz
    v2f_dir_to_light =  light_position.xyz - ( mat_model_view * vec4(position, 1) ).xyz
    v2f_normal = mat_normals * normal
and finally : 
    gl_Position = mat_mvp * vec4(position, 1)


Then for the fragment shader we followed the Phong formula  : 
    vec3 m_ads = texture2D(texture_base_color, v2f_tex_coord).rgb;
    vec3 ambientLight = (ambient * light_color) * m_ads ;
    vec3 diffuseLight = light_color  * m_ads * max( dot( normalize(v2f_normal) , normalize(v2f_dir_to_light) ) , 0.0 ) ;
    vec3 reflectDir =  reflect( normalize(-v2f_dir_to_light) , normalize(v2f_normal) ) ;
    vec3 specularLight = light_color * m_ads * pow( max( dot( normalize(reflectDir) , normalize(v2f_dir_from_view) ) , 0.0) , shininess ) ;
Where I is the final color :
    vec3 I = ambientLight + diffuseLight +  specularLight;



Issues : we had confusion to which I_l to use , is it a vector or a number ?
we used light_color which is a vec3 ...
Another problem was that some of our vectors were not normalized ,
we wasted a lot on this and ended up normalizing everything ;)


# Task 3.3    shading the Earth

this fragment shader is divided in 2 parts : 

    - compute day_color :
we followed the steps , without the clouds the color is identical to 3.2 
excpet that the specular light is scaled by a factor of : gloss * ( 1.0 - cloud )
where gloss and clouds are the values taken respectively from the gloss and cloud texture.

For the cloud color we used the same formula without the specular light.

We get the final color by mixing the two computed colors linearly interpolating them with the parameter cloud.


    - compute night_color : 
Is is just a mix of the night texture and black : 
    vec3 night_color = texture2D(texture_surface_night, v2f_tex_coord).rgb;
    vec3 final_night = mix( night_color ,  vec3(0)  , cloud );



Finally we mix the night_color and day_color linearly interpolating them with the parameter "dayness"
where "dayness" is the dot( n , l ) we use to compute the diffuse light.


Issue : It would have been nice to have a reference (in video format) to compare our results from different angles

