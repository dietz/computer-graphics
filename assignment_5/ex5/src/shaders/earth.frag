// #TASK_FILE 3
// File for task 3

//=============================================================================
//
//   Exercise code for the lecture "Introduction to Computer Graphics"
//     by Prof. Mario Botsch, Bielefeld University
//
//   Copyright (C) by Computer Graphics Group, Bielefeld University
//
//=============================================================================

precision mediump float;

varying vec2 v2f_tex_coord;
varying vec3 v2f_normal; // normal vector in camera coordinates
varying vec3 v2f_dir_to_light; // direction to light source
varying vec3 v2f_dir_from_view; // viewing vector (from eye to vertex in view coordinates)

uniform sampler2D texture_surface_day;
uniform sampler2D texture_surface_night;
uniform sampler2D texture_clouds;
uniform sampler2D texture_gloss;
uniform float sim_time;

uniform vec3  light_color;
uniform float ambient;
uniform float shininess;

void main()
{

	/** TODO 3.3:
    * - Copy your working code from the fragment shader of your Phong shader use it as
    * starting point
    * - instead of using a single texture, use the four texures `texture_surface_day`, `texture_surface_night`,
    * `texture_clouds` and `texture_gloss` and mix them for enhanced effects
    * Hints:
    * - cloud and gloss textures are just greyscales. So you'll just need one color-
    * component.
    * - The texture(texture, 2d_position) returns a 4-vector (rgba). You can use
    * `texture(...).r` to get just the red component or `texture(...).rgb` to get a vec3 color
    * value
    * - use mix(vec3 a,vec3 b, s) = a*(1-s) + b*s for linear interpolation of two colors
     */


    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    // reflected light
    vec3 reflectDir_ = reflect( normalize(-v2f_dir_to_light) , normalize(v2f_normal) ) ;
    // cloud and gloss
    float gloss = texture2D( texture_gloss , v2f_tex_coord ).r;
    float cloud = texture2D( texture_clouds , v2f_tex_coord ).r;
    float dayness = dot( normalize(v2f_normal) , normalize(v2f_dir_to_light));

    // compute day color
    vec3 day_color = texture2D(texture_surface_day, v2f_tex_coord).rgb;

    vec3 day_ambient_light = (ambient * light_color) * day_color;
    vec3 day_diffuse_light = light_color * day_color * max( dayness  , 0.0 ) ;
    vec3 day_specular_light = light_color * vec3(1,1,1) * pow( max( dot( normalize(v2f_dir_from_view)  , normalize(reflectDir_) ) , 0.0) , shininess ) ;

    vec3 final_day = day_ambient_light + day_diffuse_light + gloss * ( 1.0 - cloud ) *  day_specular_light ;

    // adding the clouds
    vec3 cloud_color =  (ambient * light_color) * vec3(cloud) + light_color * vec3(cloud) * max( dayness  , 0.0 ) ;
    vec3 final_day_cloud = mix(final_day , cloud_color , cloud );


    // compute night color
    vec3 night_color = texture2D(texture_surface_night, v2f_tex_coord).rgb;
    vec3 final_night = mix( night_color ,  vec3(0)  , cloud );
    


    ////////////////////////////////////////////
    vec3 day_night_color = mix(final_night , final_day_cloud , dayness ) ;
    ////////////////////////////////////////////
	gl_FragColor = vec4(day_night_color , 1.); 

}

